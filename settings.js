module.exports = {
  URL: "localhost",
  port: "8080",
  mongo: {
    dbURL: "mongo",
    // dbURL: "localhost",
    port: ":27017",
    dbName: "/logger"
  },
  rabbitmq: {
    host: 'amqp://rabbitmq'
  },
  mailjet:{
    MJ_APIKEY_PUBLIC:'74d72cf6a99a34095145e2181254ff94',
    MJ_APIKEY_PRIVATE:'a537cf53f86a0281bece951f27e2dcd4'
  },
  tokenSecret: "truflaLogger"
};