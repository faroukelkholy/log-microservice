var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var logsRouter = require('./routes/logs');
var app = express();
const authenticate = require('./utils/reqMiddleware').authenticate;
const MqConsumer = require("./utils/mqLogConsumer");
const settings = require('./settings');
const EmailNotifier = require('./utils/emailNotifier');
const emailNotifier = new EmailNotifier(settings.mailjet);
const MongoDriver = require('./storage/mongoDriver');
const mongoDriver = new MongoDriver(settings.mongo);
const emails = [{
  "Email": "farouk.elkholy11@gmail.com",
  "Name": "Farouk"
}]; // will be retrieved from DB

mongoDriver.onConnection().then(() => {
  mongoDriver.handleError();


  const qgetLogs = 'logs/getLog';
  const qgetLogsById = 'logs/getLogById';
  const qpostLogs = 'logs/postLog';
  const mqConsumer = new MqConsumer(mongoDriver, settings.tokenSecret, emailNotifier);
  mqConsumer.connect(settings.rabbitmq).then(() => {
    mqConsumer.serveGetLogs(qgetLogs);
    mqConsumer.serveGetLogsById(qgetLogsById);
    mqConsumer.servePostLog(qpostLogs);
  }).catch((error) => {
    console.error('mqConsumer.connect error:', error);
    emailNotifier.sendEmail(emails, 'Error Occuried', error);
  });
});


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());

app.use(authenticate);
app.use('/logs', logsRouter);



process.on('uncaughtException', async (error) => {
  console.error(`uncaughtException error: ${error}`);
  const log = {
    title: 'uncaughtException',
    description: reason,
    type: 'error',
    stackError: reason,
    errorPath: __filename
  };
  try {
    emailNotifier.sendEmail(emails, 'Error Occuried', error);
    await mongoDriver.log.saveLog(log);
  } catch (e) {
    const log = {
      title: e.name,
      description: e.message,
      type: 'error',
      stackError: e.message,
      errorPath: __filename
    };
    await mongoDriver.log.saveLog(log);
    emailNotifier.sendEmail(emails, 'Error Occuried', e);

  }
});

process.on('beforeExit', (code) => {
  console.error('Process beforeExit event with code: ', code);
});

process.on('exit', (code) => {
  console.error(`About to exit with code: ${code}`);
});

process.on('disconnect', (code) => {
  console.error(`About to disconnect with code: ${code}`);
});

process.on('unhandledRejection', async (reason, promise) => {
  console.error('Unhandled Rejection at:', promise, 'reason:', reason);
  const log = {
    title: 'unhandledRejection',
    description: reason,
    type: 'error',
    stackError: reason,
    errorPath: __filename
  };
  try {
    await mongoDriver.log.saveLog(log);
    emailNotifier.sendEmail(emails, 'Error Occuried', error);
  } catch (e) {
    const log = {
      title: e.name,
      description: e.message,
      type: 'error',
      stackError: e.message,
      errorPath: __filename
    };
    await mongoDriver.log.saveLog(log);
    emailNotifier.sendEmail(emails, 'Error Occuried', e);
  }
});

process.on('warning', (warning) => {
  console.log(warning.name); // Print the warning name
  console.log(warning.message); // Print the warning message
  console.log(warning.stack); // Print the stack trace
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  console.error(` 404 error  ${err.status || 400} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.error(` error handler ${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  if (err.response) {
    err.response.error = process.env.node_env === "development" ? err.response.error : {};
  }
  // render the error page
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err
  });
});

module.exports = app;
