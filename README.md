Log Microservice
=====

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [UseCases](#UseCases);
* [Testing](#Testing)
* [APIdocumentation](#APIdocumentation)

## General info
This project is simple Log Microservice which should store, list, and search for logs.
	
## Technologies
Project is created with:
* Node version: 10
* Mongo version: 4
* Rabbitmq
* Docker
* JWT
* Swagger
	
## Setup
To run this project, clone the code and run the docker-compose command in the project folder:

```
$ docker-compose up -d

```
> **Important:** node server tries to connect to rabbitmq before it is fully up which causes connection refused. **Work around**we are using nodemon, if we edit any file of the node code from the local machine, it will restart the server in the container. Now, The node server will be able to connect to rabbitmq.
> **possible solution** command: ["./wait-for-it.sh", "rabbitmq:5432", "--", "python", "app.py"]
## UseCases

> Some message queue use cases defined at path /utils/useCase*.js :

1. useCase1.js a client produce a message to create a log and consume a message with all the logs created, logs include the new log created

```
$ node useCase1.js
```
2. useCase2.js a client produce a message to get all logs and consume a message with all the logs

```
$ node useCase2.js
```

3. useCase3.js a client produce a message to get a log by id and consume a message the log details

```
$ node useCase3.js
```

## Testing

```
$ npm test
```

## APIdocumentation

https://app.swaggerhub.com/apis/Faroukelkholy/Trufla/1.0.0