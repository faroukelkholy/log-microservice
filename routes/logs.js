var express = require('express');
var router = express.Router();
const settings = require('../settings');
const EmailNotifier = require('../utils/emailNotifier');
const emailNotifier = new EmailNotifier(settings.mailjet);
const MongoDriver = require('../storage/mongoDriver');
const mongoDriver = new MongoDriver(settings.mongo);
mongoDriver.onConnection().then(() => {
  mongoDriver.handleError();
});

const emails = [{
  "Email": "farouk.elkholy11@gmail.com",
  "Name": "Farouk"
}]; // will be retrieved from DB

/* GET logs listing. */
router.get('/',  async (req, res) => {
  try {
    const log = {title:'Get logs listing',description:'retrieve all the logs from the DB', type:'info',protocol:'http',ReqPath:`${req.method} /logs${req.path}`,userAgent:req.headers['user-agent']};
    await mongoDriver.log.saveLog(log);
    const logsQueried = await mongoDriver.log.getLogs();
    
    return res.status(200).json({
      message: "Success",
      result: logsQueried
    });
  } catch (e) {
    console.log('error GET logs' ,e);
    const log = {title:e.name,description:e.message, type:'error',protocol:'http',statusCode:'500',ReqPath:`${req.method} /logs${req.path}`,userAgent:req.headers['user-agent'],stackError:e,errorPath:__filename};
    await mongoDriver.log.saveLog(log);
    emailNotifier.sendEmail(emails,'Error Occuried',e);
    return res.status(500).json({
      message: e.message,
      error: e
    });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const log = {title:'Get log by Id',description:'retrieve log details from the DB',type:'info',protocol:'http',ReqPath:`${req.method} /logs${req.path}`,userAgent:req.headers['user-agent']};
    await mongoDriver.log.saveLog(log);
    const logQueried = await mongoDriver.log.getLogsById(req.params.id);
    return res.status(200).json({
      message: "Success",
      result: logQueried
    });
  } catch (e) {
    console.log('error GET log by Id' ,e);
    const log = {title:e.name,description:e.message, type:'error',protocol:'http',statusCode:'500',ReqPath:`${req.method} /logs${req.path}`,userAgent:req.headers['user-agent'],stackError:e,errorPath:__filename};
    await mongoDriver.log.saveLog(log);
    emailNotifier.sendEmail(emails,'Error Occuried',e);
    return res.status(500).json({
      message: e.message,
      error: e
    });
  }
});

router.post('/', async (req, res) => {
  try {
    const log = {title:req.body.title,description:req.body.description,type:'info',protocol:'http',ReqPath:`${req.method} /logs${req.path}`,userAgent:req.headers['user-agent']};
    // emailNotifier.sendEmail(emails,'Error Occuried',log);
    await mongoDriver.log.saveLog(log);
    return res.status(201).json({
      message: "Success"
    });
  } catch (e) {
    console.log('error GET log by Id' ,e);
    const log = {title:e.name,description:e.message, type:'error',protocol:'http',statusCode:'500',ReqPath:`${req.method} /logs${req.path}`,userAgent:req.headers['user-agent'],stackError:e,errorPath:__filename};
    await mongoDriver.log.saveLog(log);
    emailNotifier.sendEmail(emails,'Error Occuried',e);
    return res.status(500).json({
      message: e.message
    });
  }
});


module.exports = router;
