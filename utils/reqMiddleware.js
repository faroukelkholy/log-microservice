const jwt = require("jsonwebtoken");
const settings = require('../settings');

async function authenticate(req, res, next) {
    if (req.method === 'OPTIONS') {
        next();
    } else if (req.headers.authorization) {
        const [type, token] = req.headers.authorization.split(" ");
        if (token) {
            jwt.verify(token, settings.tokenSecret, async (err, decodeToken) => {
                if (err) {
                    console.log("jwt verify error", err);
                    return res.status(401).json({
                        message: err
                    });
                } else {
                    next();
                }
            });

        } else {
            return res.status(401).json({
                message: "Unauthorized"
            });
        }
    } else {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }
}

module.exports = {
    authenticate: authenticate
};