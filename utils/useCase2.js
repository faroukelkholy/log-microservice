const MqConsumer = require("./mqLogConsumer");
const settings = require("../settings.js");
const MongoDriver = require('../storage/mongoDriver');
const mongoDriver = new MongoDriver(settings.mongo);

const qgetLogs = 'logs/getLog';
const qgetLogsById = 'logs/getLogById';
const qpostLogs = 'logs/postLog';
const qqueriedLogs = 'logs/queriedLogs';
const qqueriedLogsById = 'logs/queriedLog';
const qCreatedLog = 'logs/createdLog';

mongoDriver.onConnection().then(() => {
    mongoDriver.handleError();

    const mqConsumer = new MqConsumer(mongoDriver, settings.tokenSecret);

    mqConsumer.connect(settings.rabbitmq).then(() => {
        const msg = {userAgent:'Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1'};
        mqConsumer.publishReqMsg(qgetLogs, JSON.stringify(msg));
        mqConsumer.consumeQueriedLogs(qqueriedLogs);
    }).catch((error)=>{
        console.error('mqConsumer.connect error:', error);
    });
});