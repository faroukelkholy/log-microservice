const mailjet = require('node-mailjet');

class EmailNotifier {

    constructor(emailSettings) {
        this._connect(emailSettings);
    }

    _connect(emailSettings) {
       this.mailjet =  mailjet.connect(emailSettings.MJ_APIKEY_PUBLIC, emailSettings.MJ_APIKEY_PRIVATE);
    }

    sendEmail(emails,subject,error) {
        const request = this.mailjet
            .post("send", {
                'version': 'v3.1'
            })
            .request({
                "Messages": [{
                    "From": {
                        "Email": "farouk.elkholy11@gmail.com",
                        "Name": "Farouk"
                    },
                    "To": emails,
                    "Subject": subject,
                    "HTMLPart": `<h3>Dear Admin, </h3> <br /><p>Error occured that need investagation ${error.name} ${error.message} </p>`,
                    "CustomID": "AppGettingStartedTest"
                }]
            })
        request
            .then((result) => {
                console.log(result.body)
            })
            .catch((err) => {
                console.log(err.statusCode)
            })
    }
}

module.exports = EmailNotifier;