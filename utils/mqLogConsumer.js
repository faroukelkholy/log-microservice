const jwt = require("jsonwebtoken");
const amqplib = require('amqplib');
const qqueriedLogs = 'logs/queriedLogs';
const qqueriedLogsById = 'logs/queriedLog';
const qCreatedLog = 'logs/createdLog';
const ErrorHandler = require("./errorHandler");
const onErrorEvent = new ErrorHandler();



class mqClient {

    constructor(mongoDriver, tokenSecret) {
        this.mongoDriver = mongoDriver;
        this.tokenSecret = tokenSecret;
        // this.connect(rabitmqSettings);
    }

    connect(rabitmqSettings) {
        let _this = this;
        console.log('connect rabitmqSettings :', rabitmqSettings);
        return new Promise(function (resolve, reject) {
            amqplib.connect(rabitmqSettings.host).then(function (conn) {
                _this.conn = conn.createChannel();
                return resolve();
            }).catch((error) => {
                console.error('open connection error:', error);
                return reject();
            });
        });
    }

    disconnect() {
        console.log('disconnecting a mq connection');
        this.conn.close();
    }

    publishReqMsg(queue, msg) {
        let _this = this;
        this.conn.then(function (ch) {
            return ch.assertQueue(queue).then(function (ok) {
                const jsonWebToken = jwt.sign(msg, _this.tokenSecret);
                return ch.sendToQueue(queue, Buffer.from(jsonWebToken));
            });
        }).catch(console.warn);
    }

    publishResMsg(queue, msg) {
        let _this = this;
        this.conn.then(function (ch) {
            return ch.assertQueue(queue).then(function (ok) {
                return ch.sendToQueue(queue, Buffer.from(msg));
            });
        }).catch(console.warn);
    }

    serveGetLogs(queue) {
        let _this = this;
        _this.conn.then(function (ch) {
            return ch.assertQueue(queue).then(function (ok) {
                return ch.consume(queue, function (msg) {
                    if (msg !== null) {
                        console.log('serveGetLogs :', msg.content.toString());
                        jwt.verify(msg.content.toString(), _this.tokenSecret, async (err, decodeToken) => {
                            if (err) {
                                console.log("jwt verify error", err);
                                const log = {
                                    title: err.name,
                                    description: err.message,
                                    type: 'error',
                                    protocol: 'mq',
                                    statusCode: '401',
                                    ReqPath: msg.fields.routingKey,
                                    stackError: err.message,
                                    errorPath: __filename
                                };
                                await mongoDriver.log.saveLog(log);
                                _this.publishResMsg(qqueriedLogs, err);
                            } else {
                                ch.ack(msg);
                                const log = {
                                    title: 'Get logs listing',
                                    description: 'retrieve all the logs from the DB',
                                    type: 'info',
                                    protocol: 'mq',
                                    ReqPath: msg.fields.routingKey,
                                    userAgent: decodeToken.userAgent
                                };
                                await _this.mongoDriver.log.saveLog(log);
                                const logsQueried = await _this.mongoDriver.log.getLogs();
                                _this.publishResMsg(qqueriedLogs, JSON.stringify(logsQueried));
                            }
                        });

                    }
                });
            });
        }).catch(console.warn);
    }

    consumeQueriedLogs(queue) {
        let _this = this;
        _this.conn.then(function (ch) {
            return ch.assertQueue(queue).then(function (ok) {
                return ch.consume(queue, function (msg) {
                    if (msg !== null) {
                        const logs = JSON.parse(msg.content);
                        console.log('logs ', logs);
                        ch.ack(msg);
                    }
                });
            });
        }).catch(console.warn);
    }

    serveGetLogsById(queue) {
        let _this = this;
        _this.conn.then(function (ch) {
            return ch.assertQueue(queue).then(function (ok) {
                return ch.consume(queue, function (msg) {
                    if (msg !== null) {
                        console.log('serveGetLogsById JSON.parse(msg.content) :', msg.content.toString());

                        jwt.verify(msg.content.toString(), _this.tokenSecret, async (err, decodeToken) => {
                            if (err) {
                                console.log("jwt verify error", err);
                                const log = {
                                    title: err.name,
                                    description: err.message,
                                    type: 'error',
                                    protocol: 'mq',
                                    statusCode: '401',
                                    ReqPath: msg.fields.routingKey,
                                    stackError: err.message,
                                    errorPath: __filename
                                };
                                await mongoDriver.log.saveLog(log);
                                _this.publishResMsg(qqueriedLogsById, err);
                            } else {
                                ch.ack(msg);
                                const log = {
                                    title: 'Get log by Id',
                                    description: 'retrieve log details from the DB',
                                    type: 'info',
                                    protocol: 'mq',
                                    ReqPath: msg.fields.routingKey,
                                    userAgent: decodeToken.userAgent
                                };
                                await _this.mongoDriver.log.saveLog(log);
                                const logsQueried = await _this.mongoDriver.log.getLogsById(decodeToken.logId);
                                _this.publishResMsg(qqueriedLogsById, JSON.stringify(logsQueried[0]));
                            }
                        });

                    }
                });
            });
        }).catch(console.warn);
    }

    servePostLog(queue) {
        let _this = this;
        _this.conn.then(function (ch) {
            return ch.assertQueue(queue).then(function (ok) {
                return ch.consume(queue, function (msg) {
                    if (msg !== null) {
                        console.log('serveGetLogsById JSON.parse(msg.content) :', msg.content.toString());

                        jwt.verify(msg.content.toString(), _this.tokenSecret, async (err, decodeToken) => {
                            if (err) {
                                console.log("jwt verify error", err);
                                const log = {
                                    title: err.name,
                                    description: err.message,
                                    type: 'error',
                                    protocol: 'mq',
                                    statusCode: '401',
                                    ReqPath: msg.fields.routingKey,
                                    stackError: err.message,
                                    errorPath: __filename
                                };
                                await mongoDriver.log.saveLog(log);
                                _this.publishResMsg(qCreatedLog, err);
                            } else {
                                ch.ack(msg);
                                const log = {
                                    title: decodeToken.title,
                                    description: decodeToken.description,
                                    type: 'info',
                                    protocol: 'mq',
                                    ReqPath: msg.fields.routingKey,
                                    userAgent: decodeToken.userAgent
                                };
                                await _this.mongoDriver.log.saveLog(log);
                                const logsQueried = await _this.mongoDriver.log.getLogs();
                                _this.publishResMsg(qCreatedLog, JSON.stringify(logsQueried));
                            }
                        });

                    }
                });
            });
        }).catch(console.warn);
    }

}




module.exports = mqClient;