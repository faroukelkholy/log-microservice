const MqConsumer = require("./mqLogConsumer");
const settings = require("../settings.js");
const MongoDriver = require('../storage/mongoDriver');
const mongoDriver = new MongoDriver(settings.mongo);

const qgetLogsById = 'logs/getLogById';
const qqueriedLogsById = 'logs/queriedLog';


mongoDriver.onConnection().then(() => {
    mongoDriver.handleError();

    const mqConsumer = new MqConsumer(mongoDriver, settings.tokenSecret);

    mqConsumer.connect(settings.rabbitmq).then(() => {

        _this.mongoDriver.log.getLogs().then((queriedlogs) => {
            const msg = {
                logId: queriedlogs[0]._id,
                userAgent: 'Mozilla/5.0 (Linux; Android 7.0; Redmi Note 4 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.111 Mobile Safari/537.36'
            };
            mqConsumer.publishReqMsg(qgetLogsById, JSON.stringify(msg));
            mqConsumer.consumeQueriedLogs(qqueriedLogsById);
        });
    }).catch((error) => {
        console.error('mqConsumer.connect error:', error);
    });
});