

const MqConsumer = require("./mqLogConsumer");
const settings = require("../settings.js");
const MongoDriver = require('../storage/mongoDriver');
const mongoDriver = new MongoDriver(settings.mongo);

const qpostLogs = 'logs/postLog';
const qCreatedLog = 'logs/createdLog';

mongoDriver.onConnection().then(() => {
    mongoDriver.handleError();

    const mqConsumer = new MqConsumer(mongoDriver, settings.tokenSecret);

    mqConsumer.connect(settings.rabbitmq).then(() => {
        const msg = {title:'List insurance',description:'List all insurance companies',logId:'5db82ee69e40cd015f7ddd7b' ,userAgent:'Mozilla/5.0 (Linux; Android 7.0; Redmi Note 4 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.111 Mobile Safari/537.36'};
        mqConsumer.publishReqMsg(qpostLogs, JSON.stringify(msg));
        mqConsumer.consumeQueriedLogs(qCreatedLog);
    }).catch((error)=>{
        console.error('mqConsumer.connect error:', error);
    });
});