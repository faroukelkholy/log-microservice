  const mongoose = require("mongoose");
  const Schema = mongoose.Schema;

  const logSchema = Schema({
    title: String,
    description: String,
    type: String,
    protocol: String,
    statusCode: String,
    ReqPath: String,
    userAgent: String,
    stackError: Schema.Types.Mixed,
    errorPath: String,
    ts: Date
  });

  logSchema.statics.getLogs = function getLogs(user) {
    return this.find();
  };

  logSchema.statics.getLogsById = function getLogsById(id) {
    return this.find({
      _id: id
    });
  };

  logSchema.statics.getLogsByType = function getLogsType(type) {
    return this.find({
      type: type
    });
  };

  logSchema.statics.getLogsByUserAgent = function getLogsUserAgent(userAgent) {
    return this.find({
      userAgent: userAgent
    });
  };

  logSchema.statics.getLogsByProtocol = function getLogsByProtocol(protocol) {
    return this.find({
      protocol: protocol
    });
  };

  logSchema.statics.getLogsByStatusCode = function getLogsByStatusCode(statusCode) {
    return this.find({
      statusCode: statusCode
    });
  };

  logSchema.statics.saveLog = function saveLog(log) {
    const logTopersist = new this(log);
    logTopersist.ts = new Date();
    return logTopersist.save();
  };

  logSchema.statics.search = function search(query) {
    return this.find({
      "$or": [{
        title: {
          '$regex': query,
          '$options': 'i'
        }
      }, {
        description: {
          '$regex': query,
          '$options': 'i'
        }
      }, {
        stackError: {
          '$regex': query,
          '$options': 'i'
        }
      }]
    });
  };


  logSchema.index({
    type: 1
  });

  module.exports = logSchema;